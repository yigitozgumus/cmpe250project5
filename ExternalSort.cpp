/*
 //  ExternalSort.cpp
 ______                        ___   __________     ____               _           __     ______
 / ____/___ ___  ____  ___     |__ \ / ____/ __ \   / __ \_________    (_)__  _____/ /_   / ____/
 / /   / __ `__ \/ __ \/ _ \    __/ //___ \/ / / /  / /_/ / ___/ __ \  / / _ \/ ___/ __/  /___ \
 / /___/ / / / / / /_/ /  __/   / __/____/ / /_/ /  / ____/ /  / /_/ / / /  __/ /__/ /_   ____/ /
 \____/_/ /_/ /_/ .___/\___/   /____/_____/\____/  /_/   /_/   \____/_/ /\___/\___/\__/  /_____/
 /_/                                                 /___/
 //
 //  Created by Yiğit Özgümüş on 1/6/15.
 //  Copyright (c) 2015 Yigit Ozgumus. All rights reserved.
 */
#include <iomanip>
#include <stdlib.h>
#include <cstdio>
#include <string.h>

#include "ExternalSort.h"


//Methods here
//maintaining heap
void ExternalSort::Heapify(double* array, int target, int size){
    int child ;
    double temp ;
    temp = array[target];
    child = target * 2;
    while(child <= size){
        if(child < size and array[child+1]> array[child]){
            child++;
        }
        if(temp > array[child]) break ;
        else if (temp <= array[child]){
            array[child/2] = array[child];
            child *= 2;
            
        }
    }
    array[child/2] = temp ;
    return ;
}
//main sorting method
void ExternalSort::Heapsort(double* array, int size){
    double temp ;
    for (int i = size; i >= 2; i--) {
        temp = array[i];
        array[i] = array[1];
        array[1] = temp ;
        Heapify(array, 1, i-1);
    }
}
//creates the heap from array
void ExternalSort::buildMinHeap(double* array, int size){
    for(int i = size/2;i >= 1; i--){
        Heapify(array, i, size);
    }
}
//actual sorting
void ExternalSort::Sort(double *array, int size){
    buildMinHeap(array, size);
    Heapsort(array, size);
}
//Constructor for the external sort
// also all sorting job happens here
ExternalSort::ExternalSort(std::string input,std::string output){
    //input file
    std::ifstream inputFile(input);
    
    //take the total quantity
    inputFile >> MainSize;//std::cout << MainSize << std::endl;
    
    //to pointer for convenience
    stored = new double[memory+1];
    
    //Find the total number of temp files
    sizeOftmp = MainSize / memory ;
    if(MainSize % memory) sizeOftmp++;
    fileStore = new std::string[sizeOftmp];
    zeros =(sizeOftmp * memory) - MainSize;
    
    //start to take numbers from the input file
    for(int index=0;index<sizeOftmp;index++){
        char *tmpname = strdup("/tmp/tmpfileXXXXXX");
        mkstemp(tmpname);
        fileStore[index] = tmpname;
        free(tmpname);
        for (int num=1; num<=memory; num++) {
            inputFile >>std::fixed>>std::setprecision(6)>>stored[num];
        }
        Sort(stored, memory);
        std::ofstream toTemp(fileStore[index]);
        for (int num=1; num<=memory; num++)
            toTemp<<std::fixed<<std::setprecision(6)<<stored[num]<<std::endl;
        for(int i = 0;i <=memory;i++){stored[i] = 0;}
        toTemp.close();
    }
    delete [] stored;
    stored = NULL;
    inputFile.close();
    //Mergeback phase
    std::string* tempFileStore = nullptr;
    while (sizeOftmp != 1) {
        int tempSizeOftmp = sizeOftmp /2 ;
        if(sizeOftmp%2 != 0) tempSizeOftmp++;
        tempFileStore = new std::string[tempSizeOftmp];
        if (sizeOftmp%2==0) {
            for (int i=0 ;i<tempSizeOftmp ;i++) {
                char *tmpname = strdup("/tmp/tmpfileXXXXXX");
                mkstemp(tmpname);
                tempFileStore[i] = tmpname;
                free(tmpname);
                std::ofstream o (tempFileStore[i]);
                std::fstream f1(fileStore[2*i]);
                std::fstream f2(fileStore[2*i+1]);
                double tempF1,tempF2 ;
                f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                //merge sort like merging
                while (!f1.eof() and !f2.eof()) {
                    if(tempF1>tempF2){
                        o <<std::fixed<<std::setprecision(6)<< tempF2
                        <<std::endl;
                        f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                    }else{
                        o <<std::fixed<<std::setprecision(6)<< tempF1
                        <<std::endl;
                        f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                    }
                }
                while (!f1.eof()) {
                    o <<std::fixed<<std::setprecision(6)<< tempF1
                    <<std::endl;
                    f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                }
                while (!f2.eof()) {
                    o <<std::fixed<<std::setprecision(6)<< tempF2
                    <<std::endl;
                    f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                }
                std::string fileName = fileStore[2*i];
                //close the streams delete the files
                f1.close();
                f2.close();
                o.close();
                remove(fileStore[2 * i].c_str());
                remove(fileStore[2 * i +1].c_str());
            }
        }else{
            //if the files are odd numbered
            for (int i=0 ;i<tempSizeOftmp-1 ;i++) {
                char *tmpname = strdup("/tmp/tmpfileXXXXXX");
                mkstemp(tmpname);
                tempFileStore[i] = tmpname;
                free(tmpname);
                std::ofstream o (tempFileStore[i]);
                std::fstream f1(fileStore[2*i]);
                std::fstream f2(fileStore[2*i+1]);
                double tempF1,tempF2 ;
                f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                //mergesort like merging
                while (!f1.eof() and !f2.eof()) {
                    if(tempF1>tempF2){
                        o <<std::fixed<<std::setprecision(6)<< tempF2
                        <<std::endl;
                        f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                    }else{
                        o <<std::fixed<<std::setprecision(6)<< tempF1
                        <<std::endl;
                        f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                    }
                }
                while (!f1.eof()) {
                    o <<std::fixed<<std::setprecision(6)<< tempF1
                    <<std::endl;
                    f1 >>std::fixed>>std::setprecision(6)>>tempF1;
                }
                while (!f2.eof()) {
                    o <<std::fixed<<std::setprecision(6)<< tempF2
                    <<std::endl;
                    f2 >>std::fixed>>std::setprecision(6)>>tempF2;
                }
                //close the streams and delete temporary files
                f1.close();
                f2.close();
                o.close();
                remove(fileStore[2 * i].c_str());
                remove(fileStore[2 * i +1].c_str());
            }
            char *tmpname = strdup("/tmp/tmpfileXXXXXX");
            mkstemp(tmpname);
            tempFileStore[tempSizeOftmp-1] = tmpname;
            free(tmpname);
            std::ifstream LastIn(fileStore[sizeOftmp-1]);
            std::ofstream LastOut(tempFileStore[(sizeOftmp-1)/2]);
            LastOut << LastIn.rdbuf();
            LastOut.close();
            LastIn.close();
            remove(fileStore[sizeOftmp-1].c_str());
        }
        fileStore = tempFileStore ;
        sizeOftmp =tempSizeOftmp;
        
    }
    std::ofstream outFinal(output);
    std::fstream lastIn(fileStore[0]);
    std::string temp ;
    while (zeros>0) {
        std::getline(lastIn,temp);
        zeros--;
    }
    outFinal<<lastIn.rdbuf();
    outFinal.close();
    lastIn.close();
    remove(fileStore[0].c_str());
}
