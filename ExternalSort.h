//
//  ExternalSort.h
//  Cmpe250Project5
//
//  Created by Yiğit Özgümüş on 1/6/15.
//  Copyright (c) 2015 Yigit Ozgumus. All rights reserved.
//

#ifndef __Cmpe250Project5__ExternalSort__
#define __Cmpe250Project5__ExternalSort__

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>

class ExternalSort{
public:
    //Number of numbers
    int MainSize ;
    
    // The size of memory
    int memory = 1000 ;
    
    //the storage pointer
    double *stored;
    
    //Number of temporary files
    int sizeOftmp ;
    
    //number of zeros
    int zeros;
    
    //Container for temporary files
    std::string *fileStore;
    
    //Constructor for the sorting methods
    ExternalSort(std::string input,std::string output);
    //Heapsort methods
    
    //keep the heap property
    void Heapify(double *array,int target,int size);
    
    //sorting method
    void Heapsort(double *array,int size);
    
    // this methods builds min heap
    void buildMinHeap(double *array,int size);
    
    //sorts the given array
    void Sort(double *array,int size);
};

#endif /* defined(__Cmpe250Project5__ExternalSort__) */
