#include <iostream>
#include<algorithm>
#include <cstdio>
#include <string>
#include <fstream>

#include "ExternalSort.h"

int main(int argc, char *argv[]){
    
    std::string inputF = argv[1];
    std::string outputF = argv[2];
    
    ExternalSort(inputF, outputF);
    
    return 0;
}

